﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {

    public Transform target;
    public Vector3 offset;

    void LateUpdate(){
        //coloco el target en la posicion que quiero 
        transform.position = target.position + offset;
    }
}
