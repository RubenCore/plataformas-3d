﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPlataforma : MonoBehaviour
{
    public Rigidbody RBplataforma;
    public Transform[] PosicionesPlataforma;
    public float velPlat;

    private int posActual=0;
    private int posSiguiente = 1;


    void Update()
    {
        MovePlataform();
    }

    /// <summary>
    /// a partir de 3 puntos hago que se mueva la plataforma hacia esos puntos, esta preparado para poner la cantidad de puntos que quiera.
    /// para mover el pilar uso el moveTowards que calcula cuanta distancia hay entre los dos puntos especificados para no pasarse.
    /// </summary>
    void MovePlataform()
    {
        RBplataforma.MovePosition(Vector3.MoveTowards(RBplataforma.position, PosicionesPlataforma[posSiguiente].position, velPlat * Time.deltaTime));

        if (Vector3.Distance(RBplataforma.position , PosicionesPlataforma[posSiguiente].position) <= 0)
        {
            posActual = posSiguiente;
            posSiguiente++;

            if(posSiguiente> PosicionesPlataforma.Length -1)
            {
                posSiguiente = 0;
            }
        }
    }
}
