﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Buff", menuName = "new Buff", order = 1)]

public class soBuff : ScriptableObject
{
    //datos de los objetos que dan buff
    public float buffSalto;
    public float buffVel;
    public float buffCaidaPluma; 
    public float coolDown;
}
