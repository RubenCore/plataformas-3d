﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class terceraPersona : MonoBehaviour
{
    public Vector3 offset;
    public Transform target;
    [Range(0, 1)] public float lerpValue;    
    [Range(0, 10)] public float sensibilidad;
    [Range(0, 10)] public float sensibilidadY;

    void Start()
    {
        //sirve para esconder y bloquear el raton
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void LateUpdate()
    {
        //obtengo el movimiento del raton y lo multiplico por la sensivilidad
        float horiz = Input.GetAxis("Mouse X") * sensibilidad;
        float vert = Input.GetAxis("Mouse Y") * sensibilidadY;
        //a partir de un target roto la camara
        target.transform.Rotate(vert, horiz, 0);

        float dx = target.transform.eulerAngles.x;
        float dy = target.transform.eulerAngles.y;

        Quaternion q = Quaternion.Euler(dx, dy, 0);

        //suavizado de la camara con el slerp
        transform.position = Vector3.Slerp(transform.position, target.transform.position - (q * offset), lerpValue);
        //hago que mire al target
        transform.LookAt(target);
    }

    private void OnApplicationFocus(bool focus)
    {       
        Cursor.visible = false;
    }
}