﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pushRigidBody : MonoBehaviour
{
    public float fuerza = 2.0f;

    private float targetMass;

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //almaceno el el rigidbody del objeto el cual colisiono
        Rigidbody body = hit.collider.attachedRigidbody;

        //compruebo si un objeto es kinematic o no tiene rigidbody, si es asi sale de la funcion
        if(body == null || body.isKinematic)
        {
            return;
        }
        //si colisiono desde arriba salgo de la funcion para no enpujar al objeto hacia abajo 
        if(hit.moveDirection.y < -0.3)
        {
            return;
        }

        //guardo la masa del rigidbody del objeto
        targetMass = body.mass;

        //guardo la direccion de empuje 
        Vector3 direccionEmpuje = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        //aplico la velocidad a partir de la direccion de empuje multiplicado por la fuerza entre la masa del objet
        body.velocity = direccionEmpuje * fuerza/targetMass;
    }
}
