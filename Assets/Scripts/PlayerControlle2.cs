﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlle2 : MonoBehaviour
{
    public Animator animaciones;
    private Vector3 a = new Vector3(-43f, 6f, -90.5f);

    //Variables de movimiento
    [Header("Character controller")]
    private float movimientoHorizontal;
    private float movimientoVertical;
    private Vector3 playerInput;
    public CharacterController player;
    private float tpOffset;
    private Vector3 movePlayer;
    
    [Header("Variables de vel gravedad y demas")]
    //Variables de velocidad , fuerza y demas
    public float playerSpeed;
    public float gravedad;    
    public float fuerzaSalto;
    public float velCaida;

    private float actPlayerSpeed;
    private float actGravedad;
    private float actFuerzaSalto;

    [Header("CD de accion tp y salto")]
    //cd
    private bool canJump = true;
    public float timeToJump = 0.5f;
    private bool canTp = true;
    public float timeToTp = 2f;

    [Header("Camara")]
    //camara
    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;

    [Header("Variables para el control de las pendientes")]
    //variables de control de pendiente
    public bool enPendiente = false;
    private Vector3 hitNormal;
    public float slideVel;
    public float slopeForceDown;

    [Header("Variables Raycast")]
    //variables raycast
    private float limiteRaycast = 80f;
    public LayerMask ignorePlayer;

    [Header("Texto del cd")]
    private soBuff itemBuff;
    public TMPro.TextMeshProUGUI txtTime;
    private float time;

    void Start()
    {
        player = GetComponent<CharacterController>();
        Vector3 size = transform.GetChild(0).GetComponent<MeshRenderer>().bounds.size;
        tpOffset = (size.x + size.y + size.z) / 6;

        actFuerzaSalto = fuerzaSalto;
        actGravedad = gravedad;
        actPlayerSpeed = playerSpeed;

        canJump = true;
        canTp = true;

    }


    void Update()
    {
        //obtengo el movimiento a partir del horizontal y verical 
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");
        //lo guardo en un vectoy 3
        playerInput = new Vector3(movimientoHorizontal, 0, movimientoVertical);

        cameraDirection();

        //guardo hacia donde se tiene que mover el pj
        movePlayer = (playerInput.x * camRight + playerInput.z * camForward) * actPlayerSpeed;
        //le digo hacia donde tiene que mirar
        player.transform.LookAt(player.transform.position + movePlayer);

        setGravedad();

        //salto del pj ,cuanta mas fuerza de salto mas saltara
        if (player.isGrounded && Input.GetButtonDown("Jump") && canJump)
        {
            velCaida = actFuerzaSalto;
            movePlayer.y = velCaida;
            animaciones.SetTrigger("jump");
            canJump = false;
            Invoke("cdJump", timeToJump);
        }

        if (movimientoHorizontal != 0 || movimientoVertical != 0)
            animaciones.SetInteger("Walk", 1);
        else
            animaciones.SetInteger("Walk", 0);

        //tp del pj con el clic izquierdo del raton
        if (Input.GetButtonDown("Fire1") && canTp )
        {
            canTp = false;
            raycastTP();
            Invoke("cdTp", timeToTp);
        }

        //Debug.Log("Movimiento Horizontal = " + movimientoHorizontal + "Movimiento Vertical = " + movimientoVertical);
        //Debug.Log(player.velocity.magnitude);
        //print("Velocidad = " + actPlayerSpeed + " Fuerza de salto = " + actFuerzaSalto + " gravedad aplicada = " + actGravedad);
        
        if (this.transform.position.y< 0)
        {
            this.transform.position = a;
        }

        player.Move(movePlayer * Time.deltaTime);
    }

    /// <summary>
    /// cd del salto
    /// </summary>
    private void cdJump()
    {
        canJump = true;
        print("salto sin cd");
    }
    /// <summary>
    /// cd del tp 
    /// </summary>
    private void cdTp()
    {
        canTp = true;
        print("tp sin cd");
    }

    /// <summary>
    /// el tp funciona con raycas , es un raycas muy sencillito ya que solo miro a la normal en la cual hiteo para luego moverme al lado de la cara en la cual he hecho hit
    /// </summary>
    void raycastTP()
    {
        Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, limiteRaycast , ignorePlayer))
        {
            print(hit.collider.name);
            transform.position = hit.point + hit.normal * tpOffset;            
        }
    }
 
    /// <summary>
    /// aplico la gravedad
    /// </summary>
    void setGravedad()
    {
        if (player.isGrounded)
        {
           velCaida = -actGravedad * Time.deltaTime;
           movePlayer.y = velCaida;
        }
        else
        {
            velCaida -= actGravedad * Time.deltaTime;
            movePlayer.y = velCaida;
        }
        slideDown();
    }

    /// <summary>
    /// guardo la direccion de la camara , camForward es la direccion hacia delante y el camRight es la direccion hacia la derecha    
    /// </summary>
    void cameraDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;
        camForward.y = 0;
        camRight.y = 0;
        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        hitNormal = hit.normal;
    }
    /// <summary>
    /// miro si esta en una pendiente o no, se desliza mas rapido en funcion de lo inclinada que sea la pendiente
    /// </summary>
    public void slideDown()
    {
        enPendiente = Vector3.Angle(Vector3.up, hitNormal) >= player.slopeLimit;

        if (enPendiente)
        {
            movePlayer.x += ((1f- hitNormal.y)*hitNormal.x) * slideVel;
            movePlayer.z += ((1f - hitNormal.y) * hitNormal.z) * slideVel;
            movePlayer.y += slopeForceDown;
        }
    }  


    // miro la colision con el objeto que tenga el component soBuffItem 
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<soBuffItem>())
        {
            itemBuff = other.GetComponent<soBuffItem>().data;
            //si es true se suma el cooldown al tiempo , si no se queda con el cooldown , basicamente es si pillas mas de 1 buff se acumula el tiempo
            time = time > 0 ? time + itemBuff.coolDown : itemBuff.coolDown;
            addBuff();
            StartCoroutine(cdBuff());
        }       
    }
    
    /// <summary>
    /// aplico el buffo a la variable en funcion del tipo
    /// </summary>
    void addBuff()
    {
        if (itemBuff.buffVel != 0)
        {
            actPlayerSpeed += itemBuff.buffVel;
            print(" has cogido el buff vel");
        }
        if (itemBuff.buffSalto != 0)
        {
            actFuerzaSalto += itemBuff.buffSalto;
            print("has cogido el buff salto");
        }
        if (itemBuff.buffCaidaPluma != 0)
        {
            actGravedad += itemBuff.buffCaidaPluma;
            print("has cogido el buff pluma");
        }
    }

    /// <summary>
    /// cambio el texto para que se muestre en pantalla y cuando el tiempo llega a 0 aplico la funcion de resetBuffs
    /// </summary>   
    IEnumerator cdBuff()
    {
        time -= 1;
        txtTime.text = "tiempo para que termine el buff = "+ time.ToString(" 00") + " s";
        yield return new WaitForSeconds(1);

        if (time <= 0)
        {
            time = 0;
            resetBuffs();
        }else
        {
            StartCoroutine(cdBuff());
        }
             
    }

    /// <summary>
    /// reset buff iguala los valores del pj a los que tenia de un inicio
    /// </summary>
    void resetBuffs()
    {
        actFuerzaSalto = fuerzaSalto;
        actGravedad = gravedad;
        actPlayerSpeed = playerSpeed;
    }
}
